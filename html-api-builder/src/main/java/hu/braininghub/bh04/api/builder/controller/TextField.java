/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.api.builder.controller;

import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlContainer;
import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlController;

/**
 *
 * @author Attila
 */
public class TextField extends AbstractHtmlController {

    private String inputValue;
    private final InputTypes type;
    private final String name;

    public TextField(String id, String name, InputTypes type, AbstractHtmlContainer parent) {
        super(id, parent);
        this.type = type;
        this.name = name;
    }

    public String getInputValue() {
        return inputValue;
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
    }

    public InputTypes getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getHTMLCodeAsString() {
        return "<input type=\"" + type.name() + "\" value=\"" + inputValue + "\"";
    }

}
