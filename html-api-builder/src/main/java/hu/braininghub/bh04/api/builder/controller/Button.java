/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.api.builder.controller;

import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlContainer;
import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlController;

/**
 *
 *
 * @author Attila
 */
public class Button extends AbstractHtmlController {

   
    private String onClick;
    private final String label;
    
    public Button(String id, String label,AbstractHtmlContainer parent) {
        super(id,parent);
        this.label = label;
    }

    public String getOnClick() {
        return onClick;
    }

    public String getLabel() {
        return label;
    }


    
    @Override
    public String getHTMLCodeAsString() {
        return "<button type=\"button\" onclick=\""+onClick+"\">"+label+"</button>";
    }
    
}
